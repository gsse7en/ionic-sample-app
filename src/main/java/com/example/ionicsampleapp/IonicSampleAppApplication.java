package com.example.ionicsampleapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IonicSampleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(IonicSampleAppApplication.class, args);
	}
}
